
Assistant is a console app, like an information bot which pulls data according to the options specified at the launch. Such as Currency and bitcoin prices, today in history, latest news on different topics, year process in percentage etc..
Code is pretty messy at the moment, exception & error handling is completely missing which I'm working on. Other features will be implemented soon. If you wanna help and commit, you are very welcome..

You can download the jar file from the release page, open your terminal, cd to the jar directory and run java -jar Assistant.jar