package org.seferi;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.seferi.apps.*;
import org.seferi.screens.MainScreen;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static Settings settings;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (!Welcome.checkFirstRun()) {
            settings = Welcome.createSettings();
        } else {
            settings = Welcome.getSettings();
        }
        MainScreen.showMainScreen(settings);
    }
}
