package org.seferi;

import java.io.Serializable;

public class Settings implements Serializable {
    private String name;
    private String birthday;
    private String country;
    private String city;
    private String[] selectedCurrencies;

    public Settings(String name, String birthday, String country, String city, String[] selectedCurrencies) {
        this.name = name;
        this.birthday = birthday;
        this.country = country;
        this.city = city;
        this.selectedCurrencies = selectedCurrencies;
    }

    public String getName() {
        return name;
    }

    public String[] getSelectedCurrencies() {
        return selectedCurrencies;
    }

    public void setSelectedCurrencies(String[] selectedCurrencies) {
        this.selectedCurrencies = selectedCurrencies;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
