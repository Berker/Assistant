package org.seferi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SettingsTest {

    @Test
    void testObjectCreation() {
        String[] selectedCurrencies = {"USD", "RNB", "TRY"};
        Settings s = new Settings("Jack", "07 08 1968", "Germany", "Berlin", selectedCurrencies );
        assertAll("Correctly build object",
                () -> assertEquals("Jack", s.getName()),
                () -> assertEquals("07 08 1968", s.getBirthday()),
                () -> assertEquals("Germany", s.getCountry()),
                () -> assertEquals("Berlin", s.getCity())
        );

    }

    @Test
    void setNewValues() {
        String[] selectedCurrencies = {"USD", "RNB", "CAD"};
        Settings s = new Settings("Jack", "07 08 1968", "Germany", "Berlin", selectedCurrencies);
        s.setName("Billy"); s.setBirthday("09 09 1978"); s.setCountry("USA"); s.setCity("Boston");
        assertAll("Correctly sets new values",
                () -> assertEquals("Billy", s.getName()),
                () -> assertEquals("09 09 1978", s.getBirthday()),
                () -> assertEquals("USA", s.getCountry()),
                () -> assertEquals("Boston", s.getCity())
        );
    }
}