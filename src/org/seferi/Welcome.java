package org.seferi;

import org.seferi.apps.Country;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TreeSet;

public class Welcome {
    private static final String SETTINGS_FILE = System.getProperty("user.dir") + "/settings.dat";
    private static final File f = new File(SETTINGS_FILE);

    public static boolean checkFirstRun() {
        boolean check;
        return check = f.isFile();
    }

    public static Settings getSettings() throws IOException {
        try(FileInputStream fileInputStream = new FileInputStream(f)) {
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Object object = objectInputStream.readObject();
            return (Settings) object;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Settings createSettings() throws IOException, InterruptedException {
        String name, birthday,country, city;
        String[] selectedCurrencies ;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Just a few questions so I can pull the right data for your needs.");
        System.out.println("Name: ");
        name = reader.readLine();
        while(true) {
            System.out.println("Birthday (dd mm yyyy): ");
            birthday = reader.readLine();
            if (!dateValidation(birthday)) {
                System.out.println("The date you entered is not valid. Please try again.\n");
            } else { break; }
        }
        System.out.println("Country: ");

        while (!Country.isACountry(country = reader.readLine())) {
            System.out.println("That country name is not valid, please try again. \n" +
                                  "If country name contains two words, write it with \n" +
                                  "an underscore like New_Zealand.");
        }

        System.out.println("City: ");
        city = reader.readLine();
        System.out.println("Based on your country, your base currency is " + Country.getCurrencyByCountry(country) + ".\n" +
                        "Please enter two more currencies you're interested in, seperated by commas, other then your base currency, to be shown on the dashboard \n" +
                        "like EUR,USD,AUD. Here is a list of currencies: \n \nUSD: US dollar, JPY: Japanese yen, BGN: Bulgarian lev, \n" +
                        "CZK: Czech koruna, DKK: Danish krone, GBP: Pound sterling, HUF: Hungarian forint, PLN: Polish zloty, \n" +
                        "RON: Romanian leu, SEK: Swedish krona,CHF: Swiss franc,ISK: Icelandic krona, NOK: Norwegian krone, \n" +
                        "HRK: Croatian kuna, RUB: Russian rouble, TRY: Turkish lira, AUD: Australian dollar, BRL: Brazilian real, \n" +
                        "CAD: Canadian dollar, CNY: Chinese yuan renminbi, HKD: Hong Kong dollar, IDR: Indonesian rupiah, \n" +
                        "ILS: Israeli shekel, INR: Indian rupee, KRW: South Korean won, MXN: Mexican peso, MYR: Malaysian ringgit, \n" +
                        "NZD: New Zealand dollar, PHP: Philippine peso, SGD: Singapore dollar, THB: Thai baht, ZAR: South African rand");
        while(true) {
            String currency = reader.readLine().toUpperCase();
            if (!currencyValidation(currency)) {
                System.out.println("The format is wrong or not a selectable currency. Please try again.");
            } else {
                selectedCurrencies = currency.split(",");
                break;
            }
        }

        System.out.println("That's it! Thanks for the info " + name + ". Your settings will be saved as settings.dat on the same file path. Launching now!");
        Thread.sleep(1000);
        Settings settings = new Settings(name, birthday, country, city, selectedCurrencies);

        try (FileOutputStream fileOutputStream = new FileOutputStream(f)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(settings);
            objectOutputStream.close();
        }

        return settings;
    }

    public static void editSetting() {
        //TODO: Implement!
    }

    private static boolean dateValidation(String date)
    {
        boolean status = false;
        if (checkDate(date)) {
            DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
            dateFormat.setLenient(false);
            try {
                dateFormat.parse(date);
                status = true;
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }

    static boolean checkDate(String date) {
        String pattern = "(0?[1-9]|[12][0-9]|3[01]) (0?[1-9]|1[0-2]) ([0-9]{4})";
        boolean flag = false;
        if (date.matches(pattern)) {
            flag = true;
        }
        return flag;
    }

    private static boolean currencyValidation(String currencies) {
        TreeSet<String> currencyList = Country.getCurrencyList();
        if (!checkCurrencyString(currencies)) {
            return false;
        } else return currencyList.contains(currencies.split(",")[0]) && currencyList.contains(currencies.split(",")[1]);
    }

    static boolean checkCurrencyString(String str) {
        String pattern = "\\w\\w\\w(,)\\w\\w\\w";
        boolean flag = false;
        if (str.matches(pattern)) {
            flag = true;
        }
        return flag;
    }

}
