package org.seferi.apps;

import java.util.*;

public enum Country {
    AUSTRALIA("AUD"),
    AUSTRIA("EUR"),
    BELGIUM("EUR"),
    BRAZIL("BRL"),
    BULGARIA("BGN"),
    CANADA("CAD"),
    CHINA("CNY"),
    CROATIA("HRK"),
    CYPRUS("EUR"),
    CZECH_REPUBLIC("CZK"),
    DENMARK("DKK"),
    ESTONIA("EUR"),
    FINLAND("EUR"),
    FRANCE("EUR"),
    GERMANY("EUR"),
    GREAT_BRITAIN("GBP"),
    GREECE("EUR"),
    HONG_KONG("HKD"),
    HUNGARY("HUF"),
    ICELAND("ISK"),
    INDIA("INR"),
    INDONESIA("IDR"),
    IRELAND("EUR"),
    ISRAEL("ILS"),
    ITALY("EUR"),
    JAPAN("JPY"),
    LATVIA("EUR"),
    LITHUANIA("EUR"),
    LUXEMBOURG("EUR"),
    MALAYSIA("MYR"),
    MALTA("EUR"),
    MEXICO("MXN"),
    NETHERLANDS("EUR"),
    NEW_ZEALAND("NZD"),
    NORWEGIAN("NOK"),
    POLAND("PLN"),
    PORTUGAL("EUR"),
    ROMANIA("RON"),
    RUSSIA("RUB"),
    SINGAPORE("SGD"),
    SLOVAKIA("EUR"),
    SLOVENIA("EUR"),
    SOUTH_AFRICA("ZAR"),
    SOUTH_KOREA("KRW"),
    SPAIN("EUR"),
    SWEDEN("SEK"),
    SWITZERLAND("CHF"),
    THAILAND("THB"),
    TURKEY("TRY"),
    USA("USD");


    private static final Map<Country, String> BY_CURRENCY = new HashMap<>();

    static {
        for (Country c : values()) {
            BY_CURRENCY.put(c, c.currency);
        }
    }

    public final String currency;

    private Country(String currency) {
        this.currency = currency;
    }

    public static String getCurrencyByCountry(String str) {
        for (Country s : BY_CURRENCY.keySet()) {
            if (String.valueOf(s).equalsIgnoreCase(str)) {
                return s.currency;
            }
        }
        return null;
    }

    public static Country getEnumByCurrency(String currency) {
        for (Country c : BY_CURRENCY.keySet()) {
            if (c.currency.equalsIgnoreCase(currency)) {
                return c;
            }
        }
        return null;
    }

    public static TreeSet<String> getCurrencyList() {
        TreeSet<String> currencyList = new TreeSet<>();
         for (Country c : values()) {
             currencyList.add(c.currency);
         }
         return currencyList;
    }

    public static Country convertCountryToEnum(String country) {
        for (Country c : BY_CURRENCY.keySet()) {
            if (c.toString().equalsIgnoreCase(country)) {
                return c;
            }
        }
        return null;
    }

    public static boolean isACountry(String str) {
        for (Country s : BY_CURRENCY.keySet()) {
            if (String.valueOf(s).equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

}
