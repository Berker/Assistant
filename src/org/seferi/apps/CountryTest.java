package org.seferi.apps;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountryTest {

    @Test
    void valueOfCurrency() {
        assertEquals(Country.GERMANY.currency, Country.getCurrencyByCountry("Germany"));
    }

    @Test
    void values() {
        assertEquals(Country.USA, Country.valueOf("USA"));
    }

    @Test
    void valueOf() {
        assertEquals(Country.USA, Country.valueOf("USA"));
    }

    @Test
    void isACountry() {
        assertTrue(Country.isACountry("USA"));
    }
}