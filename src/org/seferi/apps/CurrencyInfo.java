package org.seferi.apps;

import com.google.gson.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class CurrencyInfo {
    public static Map<String, String> getAllRatesByBaseCurrency(Country country) throws IOException {
        String globalURL = "https://api.exchangeratesapi.io/latest?base=" + country.currency;
        URL url = new URL(globalURL);
        URLConnection request = url.openConnection();
        Map<String, String> currencyMap = new HashMap<>();
        TreeSet<String> currencyList = Country.getCurrencyList();
        currencyList.remove(country.currency);

        JsonObject jsonObject = JsonParser.parseReader((new InputStreamReader((InputStream) request.getContent()))).getAsJsonObject();
        JsonObject data = (JsonObject) jsonObject.get("rates");

        for (String s : currencyList)
            currencyMap.put(s.trim(), data.getAsJsonObject().get(s).toString());
        return currencyMap;
    }

    public static String calculateRatesOnCurrency(Country firstCountry, Country secondCountry, int amount) throws IOException {
        Map<String, String> ratesMap = getAllRatesByBaseCurrency(firstCountry);
        double rate = Double.parseDouble(ratesMap.get(secondCountry.currency));
        double result = amount * rate;
        return String.valueOf(result);
    }

    public static String getBitcoinRateByCurrency(Country country) throws IOException {
        String globalURL = "https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=" + country.currency.toLowerCase();
        URL url = new URL(globalURL);
        URLConnection request = url.openConnection();

        JsonObject jsonObject = JsonParser.parseReader((new InputStreamReader((InputStream) request.getContent()))).getAsJsonObject();
        JsonPrimitive data = (JsonPrimitive) jsonObject.get("bitcoin").getAsJsonObject().get("eur");
        return data.toString();
    }
}
