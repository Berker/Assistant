package org.seferi.apps;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyInfoTest {

    @Test
    void getAllRatesByBaseCurrency() {
    }

    @Test
    void calculateRatesOnCurrency() throws IOException {
        assertEquals("2.4254", CurrencyInfo.calculateRatesOnCurrency(Country.GERMANY, Country.USA, 2));
    }
}