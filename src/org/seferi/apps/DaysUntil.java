package org.seferi.apps;

import org.seferi.Settings;
import org.seferi.Welcome;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DaysUntil {
    //Counts the days until certain dates like birthday, christmas etc...


    public static String daysUntilBirthday(Settings settings) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        String birthday = settings.getBirthday();
        String[] dateArray = birthday.split(" ");
        String today = LocalDate.now().format(dtf);
        String parsedBirthday = dateArray[0] + " " + dateArray[1] + " " + LocalDate.now().getYear();
        LocalDate date1 = LocalDate.parse(parsedBirthday, dtf);
        LocalDate date2 = LocalDate.parse(today, dtf);
        long daysBetween = ChronoUnit.DAYS.between(date2, date1);                                     //  long daysBetween = Duration.between(date1, date2).toDays();
        return String.valueOf(daysBetween);
    }

    public static String daysUntilChristmas() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        String christmasDay = "25 12 " + LocalDate.now().getYear();
        String today = LocalDate.now().format(dtf);
        LocalDate date1 = LocalDate.parse(christmasDay, dtf);
        LocalDate date2 = LocalDate.parse(today, dtf);
        long daysBetween = ChronoUnit.DAYS.between(date2, date1);                                     //  long daysBetween = Duration.between(date1, date2).toDays();
        return String.valueOf(daysBetween);
    }
}
