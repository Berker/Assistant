package org.seferi.apps;

public class Event {
    private String shortDescription;
    private String longDescription;
    private String eventURL;

    public Event(String shortDescription, String longDescription, String eventURL) {
        this.shortDescription = shortDescription;
        this.longDescription = longDescription;
        this.eventURL = eventURL;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getEventURL() {
        return eventURL;
    }

    public void setEventURL(String eventURL) {
        this.eventURL = eventURL;
    }
}
