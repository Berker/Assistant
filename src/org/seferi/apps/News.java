package org.seferi.apps;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class News {

    public static ArrayList<NewsArticle> getHeadlines(String category) throws IOException {
        ArrayList<NewsArticle> articlesList = new ArrayList<>();
        String newsUrl = "https://saurav.tech/NewsAPI/top-headlines/category/" + category + "/us.json";
        URL urlAdress = new URL(newsUrl);
        URLConnection request = urlAdress.openConnection();

        JsonObject jsonObject = JsonParser.parseReader((new InputStreamReader((InputStream) request.getContent()))).getAsJsonObject();
        JsonArray data = jsonObject.getAsJsonArray("articles");

        for (JsonElement element : data) {
            String sourceName = element.getAsJsonObject().get("source").getAsJsonObject().get("name").toString();
            String title = element.getAsJsonObject().get("title").toString();
            String description = element.getAsJsonObject().get("description").toString();
            String url = element.getAsJsonObject().get("url").toString();
            String publishedAt = element.getAsJsonObject().get("publishedAt").toString();
            String content = element.getAsJsonObject().get("content").toString();

            articlesList.add(new NewsArticle(sourceName, title, description, url, publishedAt, content));
        }
        return articlesList;
    }
}
