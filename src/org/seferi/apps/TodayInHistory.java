package org.seferi.apps;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;

/* Pulls the today in history selected data from wikipedia */
public class TodayInHistory {

    public static ArrayList<Event> getAllSelectedEvents() throws IOException {
        LocalDate today = LocalDate.now();
        String[] todayStr = today.toString().split("-");
        String globalURL = "https://en.wikipedia.org/api/rest_v1/feed/onthisday/selected/" + todayStr[1] + "/" + todayStr[2];
        URL url = new URL(globalURL);
        URLConnection request = url.openConnection();
        ArrayList<Event> eventsList = new ArrayList<>();

        JsonObject jsonObject = JsonParser.parseReader((new InputStreamReader((InputStream) request.getContent()))).getAsJsonObject();
        JsonArray data = (JsonArray) jsonObject.get("selected");

        for (JsonElement element : data) {
            String shortDescription = element.getAsJsonObject().get("text").toString();
            String longDescription = element.getAsJsonObject().get("pages").getAsJsonArray().get(0).getAsJsonObject().get("extract").toString();
            String eventURL = element.getAsJsonObject().get("pages").getAsJsonArray().get(0).getAsJsonObject().get("content_urls").getAsJsonObject().get("desktop").getAsJsonObject().get("page").toString();
            eventsList.add(new Event(shortDescription, longDescription, eventURL));
        }
        return eventsList;
    }


}
