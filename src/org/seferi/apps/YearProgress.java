package org.seferi.apps;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

// Show how many percent of the year elapsed..
public class YearProgress {
    public static String getYearProgress() {
        DecimalFormat df = new DecimalFormat("0.0");
        float hoursInYear = (LocalDate.now().lengthOfYear()) * 24;
        float todayInHours = ((LocalDateTime.now().getDayOfYear() - 1) * 24) + LocalDateTime.now().getHour();
        return df.format((todayInHours * 100) / hoursInYear) + "%";
    }
}
