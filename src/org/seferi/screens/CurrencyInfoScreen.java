package org.seferi.screens;

import org.seferi.Settings;
import org.seferi.Welcome;
import org.seferi.apps.Country;
import org.seferi.apps.CurrencyInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.LinkPermission;
import java.text.DecimalFormat;
import java.util.Map;

public class CurrencyInfoScreen {
    public static void showAllCurrencyExchangeRates() throws IOException, InterruptedException {
        Settings settings = Welcome.getSettings();
        Map<String, String> ratesMap = CurrencyInfo.getAllRatesByBaseCurrency(Country.convertCountryToEnum(settings.getCountry()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("                                    +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+\n" +
                "                                    |C|u|r|r|e|n|c|y| |E|x|c|h|a|n|g|e| |R|a|t|e|s|\n" +
                "                                    +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+ +-+-+-+-+-+");
        System.out.println();
        int count = 0;
        for (Map.Entry<String, String> entry : ratesMap.entrySet()) {
            System.out.print("                      " + entry.getKey() + ": " + entry.getValue());
            count++;
            if (count == 3) {
                System.out.println();
                count = 0;
            }
        }
        System.out.println("\n");

        System.out.println("| MENU: 1- From/to currency calculation | 2 - Return to dashboard | 0- Exit |");
        String input = reader.readLine();
        switch (input) {
            case "1":
                calculateCurrency();
                break;
            case "2":
                MainScreen.showMainScreen(settings);
                break;
            default:
                System.out.println("Goodbye " + settings.getName());
                Thread.sleep(1000);
                System.exit(0);
        }

    }

    private static void calculateCurrency() throws IOException {
        String from, to;
        int amount;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Enter base currency (like EUR): ");
        from = reader.readLine();
        Map<String, String> rateMap = CurrencyInfo.getAllRatesByBaseCurrency(Country.getEnumByCurrency(from));


        System.out.println("Enter a currency you want to convert to (like USD): ");
        to = reader.readLine();
        System.out.println("Enter an amount (like 42): ");
        amount = Integer.parseInt(reader.readLine());
        System.out.println("Total: " + df.format(amount * Double.parseDouble(rateMap.get(to))));

    }
}
