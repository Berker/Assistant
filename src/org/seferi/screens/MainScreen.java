package org.seferi.screens;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.seferi.Settings;
import org.seferi.Welcome;
import org.seferi.apps.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;

public class MainScreen {



public static void showMainScreen(Settings settings) throws IOException, InterruptedException {
    LocalDate today = LocalDate.now();
    ArrayList<Event> eventsList = TodayInHistory.getAllSelectedEvents();
    ArrayList<NewsArticle> newsList = News.getHeadlines("general");
    Map<String, String> currencyMap = CurrencyInfo.getAllRatesByBaseCurrency(Country.convertCountryToEnum(settings.getCountry()));
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    System.out.println("                                          ___          _     _              _   \n" +
            "                                         / _ \\        (_)   | |            | |  \n" +
            "                                        / /_\\ \\___ ___ _ ___| |_ __ _ _ __ | |_ \n" +
            "                                        |  _  / __/ __| / __| __/ _` | '_ \\| __|\n" +
            "                                        | | | \\__ \\__ \\ \\__ \\ || (_| | | | | |_ \n" +
            "                                        \\_| |_/___/___/_|___/\\__\\__,_|_| |_|\\__|\n" +
            "                                                                                \n" +
            "                                                                                \n");

    System.out.println("--------------------------------------------------------------------------------------------------------------------");
    System.out.print("|  Date: " + today.getDayOfMonth() + " " + today.getMonth() + " " + today.getYear());
    System.out.print("  |  Days until your birthday: " + DaysUntil.daysUntilBirthday(settings));
    System.out.print("  |  Days until christmas: " + DaysUntil.daysUntilChristmas());
    System.out.print("  |  Year progress: " + YearProgress.getYearProgress() + "  |");
    System.out.println();
    System.out.println("--------------------------------------------------------------------------------------------------------------------");
    System.out.print("|  Base Currency: " + Country.getCurrencyByCountry(settings.getCountry()));
    System.out.print("  |  " + settings.getSelectedCurrencies()[0] + ": " + currencyMap.get(settings.getSelectedCurrencies()[0]));
    System.out.print("  |  " + settings.getSelectedCurrencies()[1] + ": " + currencyMap.get(settings.getSelectedCurrencies()[1]));
    System.out.print("  |  " + "Bitcoin: " + CurrencyInfo.getBitcoinRateByCurrency(Country.convertCountryToEnum(settings.getCountry())) + "  |");
    System.out.println("\n--------------------------------------------------------------------------------------------------------------------");

    System.out.println("                                             ____                         \n" +
            "                                              /   _/_    '   / ' __/  _   \n" +
            "                                             ( ()(/(/(/ //) /)/_) /()/ (/ \n" +
            "                                                     /                 /  \n");
    System.out.println("--------------------------------------------------------------------------------------------------------------------");
    for (int j = 0; j < 3; j++) {
        System.out.print("# ");
        String eventDesc = eventsList.get(j).getShortDescription();
        for (int i = 0; i < eventDesc.length(); i++) {
            System.out.print(eventDesc.charAt(i));
            if (i == 113) {
                System.out.println();
            }
        }
        System.out.println();
    }
    System.out.println();

    System.out.println("--------------------------------------------------------------------------------------------------------------------");

    System.out.println("                             __      __   ____  ____  ___  ____    _  _  ____  _    _  ___ \n" +
            "                            (  )    /__\\ (_  _)( ___)/ __)(_  _)  ( \\( )( ___)( \\/\\/ )/ __)\n" +
            "                             )(__  /(__)\\  )(   )__) \\__ \\  )(     )  (  )__)  )    ( \\__ \\\n" +
            "                            (____)(__)(__)(__) (____)(___/ (__)   (_)\\_)(____)(__/\\__)(___/");
    System.out.println("--------------------------------------------------------------------------------------------------------------------");
    System.out.println();

    for (int j = 0; j < 5; j++) {
        System.out.print("# ");
        String newsDesc = newsList.get(j).getTitle();
        for (int i = 0; i < newsDesc.length(); i++) {
            System.out.print(newsDesc.charAt(i));
            if (i == 113) {
                System.out.println();
            }
        }
        System.out.println();
    }

    System.out.println();

    System.out.println("| 1- Today in history | 2- Currencies | 3- News | 0- Exit |");

    String input = reader.readLine();
    switch (input) {
        case "1":
            TodayInHistoryScreen.showDetails();
            break;
        case "2":
            CurrencyInfoScreen.showAllCurrencyExchangeRates();
            break;
        case "3":
            NewsScreen.showNews("general");
            break;
        case "0":
            System.out.println("Goodbye " + settings.getName());
            Thread.sleep(1000);
            System.exit(0);
    }

}

}
