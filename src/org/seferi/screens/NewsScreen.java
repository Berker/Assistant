package org.seferi.screens;

import org.seferi.apps.News;
import org.seferi.apps.NewsArticle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static org.seferi.Main.settings;

public class NewsScreen {
    public static void showNews(String category) throws IOException, InterruptedException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<NewsArticle> newsList = News.getHeadlines(category);
        System.out.println("--------------------------------------------------------------------------------------------------------------------");

        System.out.println("                             __      __   ____  ____  ___  ____    _  _  ____  _    _  ___ \n" +
                "                            (  )    /__\\ (_  _)( ___)/ __)(_  _)  ( \\( )( ___)( \\/\\/ )/ __)\n" +
                "                             )(__  /(__)\\  )(   )__) \\__ \\  )(     )  (  )__)  )    ( \\__ \\\n" +
                "                            (____)(__)(__)(__) (____)(___/ (__)   (_)\\_)(____)(__/\\__)(___/");
        System.out.println("--------------------------------------------------------------------------------------------------------------------");
        System.out.println();

        for (int j = 0; j < newsList.size(); j++) {
            System.out.print("# ");
            String newsDesc = newsList.get(j).getDescription();
            System.out.print(newsList.get(j).getTitle());
            System.out.println();
            for (int i = 0; i < newsDesc.length(); i++) {
                System.out.print(newsDesc.charAt(i));
                if (i == 113) {
                    System.out.println();
                }
            }
            System.out.println();
            System.out.println(newsList.get(j).getUrl());
            System.out.println();
        }

        System.out.println();

        System.out.println("| 1- General | 2- Business | 3- Entertainment | 4- Health | 5- Science | 6- Sports | 7- Tech | 8- Dashboard | 0- Exit |");

        String input = reader.readLine();
        switch (input) {

            case "2":
                showNews("business");
                break;
            case "3":
                showNews("entertainment");
                break;
            case "4":
                showNews("health");
                break;
            case "5":
                showNews("science");
                break;
            case "6":
                showNews("sports");
                break;
            case "7":
                showNews("technology");
                break;
            case "8":
                MainScreen.showMainScreen(settings);
                break;
            case "0":
                System.out.println("Goodbye " + settings.getName());
                System.exit(0);
                break;
            default:
                showNews("general");
        }
    }
}
