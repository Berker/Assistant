package org.seferi.screens;

import org.seferi.Settings;
import org.seferi.Welcome;
import org.seferi.apps.Event;
import org.seferi.apps.TodayInHistory;

import javax.swing.plaf.TableHeaderUI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class TodayInHistoryScreen {
    public static void showDetails() throws IOException, InterruptedException {
        Settings settings = Welcome.getSettings();
        ArrayList<Event> eventsList = TodayInHistory.getAllSelectedEvents();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("                                             ____                         \n" +
                "                                              /   _/_    '   / ' __/  _   \n" +
                "                                             ( ()(/(/(/ //) /)/_) /()/ (/ \n" +
                "                                                     /                 /  \n");
        System.out.println("--------------------------------------------------------------------------------------------------------------------");
        for (int j = 0; j < eventsList.size(); j++) {
            int counter = 0;
            //System.out.print("# ");
            Event event = eventsList.get(j);
            for (int i = 0; i < event.getLongDescription().length(); i++) {
                System.out.print(event.getLongDescription().charAt(i));
                counter++;
                if (counter == 113) {
                    System.out.println();
                    counter = 0;
                }
            }
            System.out.println();
            System.out.println("Link to the Wikipedia article: " + event.getEventURL());
            System.out.println();
        }
        System.out.println("| MENU: 1- Back to dashboard | 0- Exit |");

        switch (reader.readLine()) {
            case "1":
                MainScreen.showMainScreen(settings);
                break;
            case "0":
                System.out.println("Goodbye " + settings.getName());
                Thread.sleep(1000);
                System.exit(0);
                break;
            default:
                System.exit(0);
        }
    }
}
